﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodificadorCovulacional
{
    class CustomStateMachine : StateMachine
    {

        public override string ReceivedInput(ref List<int> state, int input)
        {
            List<int> aux = new List<int>() { { input},{ state[0] },{ state[1] } };
            state = new List<int>() { { aux[0] }, { aux[1] } };

            StringBuilder output = new StringBuilder();
            output.Append(aux[0] == aux[2] ? '0' : '1');
            output.Append( (aux[0] == aux[2] ? 0 : 1) == aux[1] ? '0' : '1');

            return output.ToString();
        }

    }
}
