﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodificadorCovulacional
{
    abstract class StateMachine
    {
        protected List<int> state;

        protected StateMachine()
        {
            state = new List<int>() { { 0 }, { 0 }, { 0 } };
        }


        public abstract string ReceivedInput( ref List<int> state, int input);
        //public abstract string WhenOne( ref List<int> state);

    }
}
