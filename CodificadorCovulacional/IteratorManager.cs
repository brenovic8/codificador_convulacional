﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodificadorCovulacional
{
    static class IteratorManager
    {

        public static String RunIteration( StateMachine machine, string value)
        {
            StringBuilder output = new StringBuilder();
            List<int> state = new List<int>() { { 0 }, { 0 } };

            for (int i = value.Length-1; i >= 0; i--)
                output.Insert(0, machine.ReceivedInput( ref state, Int32.Parse( value[i].ToString())))  ;
            
            
                return output.ToString();
        }
    }
}
