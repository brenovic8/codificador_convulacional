﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodificadorCovulacional
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomStateMachine cm = new CustomStateMachine();


            // 010101000001 => 010001000111000000110111
            // 110101000001 => 100001000111000000110111


            var output = IteratorManager.RunIteration(cm, "110101000001");
            Console.WriteLine( output);

            Console.ReadKey();
        }
    }
}
